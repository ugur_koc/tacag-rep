Tacag-Rep
=========

Test case Aware Covering Array Generator
In combinatorial interaction testing, test case-aware covering arrays aim to 
ensure that each test case has a fair chance to test all of its valid t-tuples of 
configuration options. Constructing Covering Array is known to be a NP-Hard 
problem. When we try to augment covering array to be aware of test cases the 
construction problem even gets harder. Therefore, we used Simulated Annealing 
algorithm to solve this problem. Empirical results of our study showed that we 
have significantly reduced covering array construction time
